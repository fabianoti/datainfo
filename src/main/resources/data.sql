INSERT INTO funcao_usuario_externo(co_funcao, no_funcao) VALUES (1, 'Gestor');
INSERT INTO funcao_usuario_externo(co_funcao, no_funcao) VALUES (2, 'Administrador');
INSERT INTO funcao_usuario_externo(co_funcao, no_funcao) VALUES (3, 'Frente de Caixa');

INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('1', '95578048072', 'fabianodecastromonteiro@outlook.com', 'Fabiano de Castro Monteiro', '1', 'A', '48996578776', '1');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('2', '11111111111', 'fabiano@teste1.com', 'Fabiano - Teste 01', '1', 'I', '11111111111', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('3', '22222222222', 'fabiano@teste2.com', 'Fabiano - Teste 02', '1', 'I', '22222222222', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('4', '33333333333', 'fabiano@teste3.com.br', 'Fabiano - Teste 03', '1', 'A', '33333333333', '3');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('5', '44444444444', 'fabiano@teste4.sc.br', 'Fabiano - Teste 04', '1', 'I', '44444444444', '3');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('6', '55555555555', 'fabiano@teste5.com', 'Fabiano - Teste 05', '1', 'A', '55555555555', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('7', '22222222222', 'fabiano@teste6.com', 'Fabiano - Teste 06', '1', 'I', '66666666666', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('8', '33333333333', 'fabiano@teste7.com.br', 'Fabiano - Teste 07', '1', 'A', '77777777777', '3');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('9', '44444444444', 'fabiano@teste8.sc.br', 'Fabiano - Teste 08', '1', 'I', '88888888888', '3');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('10', '55555555555', 'fabiano@teste9.com', 'Fabiano - Teste 09', '1', 'A', '99999999999', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('11', '55555555555', 'fabiano@teste10.com', 'Fabiano - Teste 10', '1', 'A', '10101010101', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('12', '55555555555', 'fabiano@teste11.com', 'Fabiano - Teste 11', '1', 'A', '10111011011', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('13', '55555555555', 'fabiano@teste12.com', 'Fabiano - Teste 12', '1', 'A', '12121212121', '2');
INSERT INTO usuario_externo (co_usuario, nu_cpf, de_email, no_usuario, ic_perfil_acesso, ic_situacao, nu_telefone, co_funcao) VALUES ('14', '55555555555', 'fabiano@teste13.com', 'Fabiano - Teste 13', '1', 'A', '13131313131', '2');
