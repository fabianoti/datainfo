angular.module("apps").controller("HomeController", function($scope, $location, api, $routeParams){
    
	$scope.ano = {};
	
	$scope.getAno = function(){

		var data = new Date();
		var anoAtual = data.getFullYear();
		
		$scope.ano = anoAtual;
	}
	
	$scope.getAno();
	
});