angular.module("apps").controller("UsuarioExternoListaController", 
		function($scope, api, utils, $uibModal, toastr, $filter, $location){

	$scope.usuariosFiltro = {
//		situacoes: {
//			“Todos”, 
//			“Habilitado”,
//			“Desabilitado			
//		}	
	};
    $scope.paginaAtual = 1;
    $scope.limite = 10;
        
	$scope.isOpen = false;
	
	$scope.usuariosFiltro = {};
	
	$scope.openMenu = function() {
		$scope.isOpen = true;
	}
	
	api.datainfo.usuario.getAll().then(function( response ) {
		$scope.usuarios = response.data;
	});

	$scope.visualizaRegistro = function (id, size) {
		api.datainfo.usuario.getById(id).then(function(response) {
			var modalInstance = $uibModal.open({ 
				templateUrl: "modulos/datainfo/usuario.externo.formulario.html", 
				controller: "ModalUsuarioExternoFormularioController",				
				backdrop: 'static', 
				keyboard: false,
				size:size,
				resolve: { 
			    	usuario: function () {
			    		return response.data;
			        }
			      }
			});
		}, function (error) {
			
		}, function (value) {
			
		});
	}
	
	$scope.remover = function (id) {
		api.datainfo.usuario.excluir(id).then(function(response){
//			$scope.usuarios.splice($scope.usuarios.indexOf(response.data), 1);
			toastr.success('Registro removido com sucesso');
		});
	}
	
	$scope.removerUsuario = function (usuario) {
		var mensagem = "Confirma exclusão deste Registro"+"?";
		var modalInstance = $uibModal.open ({ 
			templateUrl: "modulos/template/confirma.exclusao.html", 
			controller: "ModalConfirmaExclusaoController",
			resolve: { 
		    	mensagem: function () {
		    		return mensagem;
		        },
		        id: function(){
		        	return usuario.id;
		        }
			}
		});
		
		modalInstance.result.then(function (id) {
			$scope.remover(id);
		});
	}
	
/*
	$scope.mensagemAlerta = function( mensagem ){
		var modalInstance = $uibModal.open({ 
			templateUrl: "modulos/template/dialog.mensagem.html", 
			controller: "ModalMensagemDialogController",
			
			resolve: { 
				mensagem: function () {
		    		return mensagem;
		        },
				titulo: function(){
					return null;
				}
		      }
		});
	}
	*/
	//$scope.getAll();	
	
});