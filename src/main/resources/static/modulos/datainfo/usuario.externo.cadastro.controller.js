angular.module("apps").controller('UsuarioExternoCadastroController', function ($scope, api, $uibModal, toastr, $routeParams, $location) {
	
	$scope.usuario;

	api.datainfo.funcao.getAll().then(function( response ) {
		$scope.funcoes = response.data;
	});

	$scope.salvarUsuario = function(usuario) {
		api.datainfo.usuario.insert(usuario).then(function(response) {
			//$scope.ficha.animais.splice($scope.ficha.animais.indexOf(response.data.data), 1);
			toastr.success('Usuario incluído com sucesso!');
		});
	}
		
//	$scope.addUsuario = function(size) {
//		var modalInstance = $uibModal.open({ 
//			templateUrl: "modulos/datainfo/usuario.externo.cadastro.html", 
//			controller: "UsuarioExternoCadastroController",
//			usuario: usuario,
//			backdrop: 'static', 
//			keyboard: false,
//			size:size,
//			resolve: {
//				usuarios: function() {
//					return null;
//				}
//			}
//		});
//		
//		modalInstance.result.then(function (usuarios) {
//			if (usuario != null) {
//				$scope.ficha.animais.push(animais);
//			}
//		});
//	}
//	
//	$scope.remover = function(id) {
//		api.epizootia.animal.excluir(id).then(function(response){
//			$scope.ficha.animais.splice($scope.ficha.animais.indexOf(response.data.data), 1);
//			toastr.success('Usuario removido com sucesso!');
//		});
//	}
//	
//	$scope.removerAnimal = function(fichaAnimal){
//		var mensagem = "Confirma a exclusão do animal ?";
//		var modalInstance = $uibModal.open({ 
//			templateUrl: "modulos/template/confirma.exclusao.html", 
//			controller: "ModalConfirmaExclusaoController",
//			resolve: { 
//				mensagem: function () {
//					return mensagem;
//				},
//				id: function(){
//					return fichaAnimal.id;
//				}
//			}
//		});
//	
//		modalInstance.result.then(function (id){
//			$scope.remover( id );
//		});
//	}
//			
//	$scope.visualizaAnimal = function(fichaAnimal, size){
//		var modalInstance = $uibModal.open({ 
//			templateUrl: "modulos/epizootia/visualiza.macaco.html", 
//			controller: "ModalVisualizaAnimalController",
//			backdrop: 'static', 
//			keyboard: false,
//			size:size,
//			resolve: { 
//		    	animal: fichaAnimal
//		      }
//		});
//	}, function(error) {
//		
//	}, function(value) {
//		// });
//	}
//		
//	$scope.addMorador = function(){
//		var modalInstance = $uibModal.open({ 
//			templateUrl: "modulos/epizootia/cadastro.morador.html", 
//			controller: "CadastroMoradorController",
//
//			backdrop: 'static', 
//			keyboard: false,
//			resolve: {
//				amostra: function(){
//					return null;
//				}
//			}
//		});
//		
//		modalInstance.result.then(function ( localMorador ){
//			if(localMorador != null) {
//				$scope.moradores.push( localMorador );
//			}
//		});
//	}
//	
//	$scope.addCorpoAgua = function(){
//		var modalInstance = $uibModal.open({ 
//			templateUrl: "modulos/epizootia/cadastro.corpoAgua.html", 
//			controller: "CorpoAguaController",
//			backdrop: 'static', 
//			keyboard: false,
//			resolve: {
//				amostra: function(){
//					return null;
//				}
//			}
//		});
//		
//		modalInstance.result.then(function ( localCorpoAgua ){
//			if(localCorpoAgua != null) {
//				$scope.corposAguas.push( localCorpoAgua );
//			}
//		});
//	}
//
//	$scope.addUnidade = function() {
//		var modalInstance = $uibModal.open({ 
//			templateUrl: "modulos/epizootia/cadastro.unidadeConservacao.html", 
//			controller: "UnidadeConservacaoController",
//			backdrop: 'static', 
//			keyboard: false,
//			resolve: {
//				amostra: function() {
//					return null;
//				}
//			}
//		});
//		
//		modalInstance.result.then(function(unidadesConservacao) {
//			if (unidadesConservacao != null) {
//				$scope.unidadesConservacao.push( unidadesConservacao );
//			}
//		});
//	} 

});