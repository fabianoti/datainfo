angular.module("apps").controller("ModalUsuarioExternoFormularioController", function($scope, api, $uibModalInstance, animal ){
	
	$scope.animal = animal;
	
	api.datainfo.funcoes.getAll().then(function(response) {
		$scope.funcoes = response.data;
	});

//	api.epizootia.anormalidade.getAll().then(function(response) {
//		$scope.anormalidades = response.data;
//	});
//	
	$scope.close = function() {
		$uibModalInstance.dismiss();
	}
	
});