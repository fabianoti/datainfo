angular.module("apps")
.filter('parseBoolean', function(){
	return function(valor){
		return valor ? "Sim" : "Não";
	};
})
.filter("formatNumber",function(){
	return function(valor){
		return valor < 0 ? "--" : valor;
	}
});