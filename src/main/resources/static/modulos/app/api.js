angular.module("apps").factory("api", function($http, $window, $filter) {

	return {
		datainfo: {
			usuario: {
				insert: function(usuario) {
					return $http.post("./rest/datainfo/usuario-externo", usuario);
				},
				getAll: function() {
					return $http.get("./rest/datainfo/usuario-externo/all");
				},
				getById: function(id) {
					return $http.get("./rest/datainfo/usuario-externo/"+ id);
				},
				/*
				update: function(usuario) {
					return $http.put("./rest/servicosinfo/entrada-equipamento", usuario);
				},
				*/
				excluir: function(id) {
					return $http.delete("./rest/datainfo/usuario-externo/"+ id );
				}				
			},			
			funcao: {
				getAll: function() {
					return $http.get("./rest/datainfo/funcao-usuario-externo/all");
				},
				getById: function(id) {
					return $http.get("./rest/datainfo/funcao-usuario-externo/"+ id);
				}				
			}			
		}		
	}
});