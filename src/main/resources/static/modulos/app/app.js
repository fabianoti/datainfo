
var app = angular.module("apps",
		[ 'ngRoute', 'ngSanitize', 
		  'ui.bootstrap', 
		  'toastr', 
		  'picardy.fontawesome', 
		  'mgcrea.ngStrap', 
		  'angularFileUpload', 
		  'ui.utils.masks', 
		  'brasil.filters', 
		  'ngTagsInput']);

app.config(function($routeProvider,  toastrConfig){
	
	angular.extend(toastrConfig, {
	    positionClass: 'toast-bottom-right',
	    target: 'body'
	});
	
   $routeProvider
        .when('/home', {
           templateUrl: 'modulos/home/home.html', 
           controller: 'HomeController'
       })
       .when('/datainfo/usuario/listar', {
           templateUrl: 'modulos/datainfo/usuario.externo.lista.html', 
           controller: 'UsuarioExternoListaController'
       })
       .when('/datainfo/usuario/cadastrar', {
           templateUrl: 'modulos/datainfo/usuario.externo.cadastro.html', 
           controller: 'UsuarioExternoCadastroController'
       })
       .otherwise( {redirectTo: '/home' })
});

app.run( function(uibPaginationConfig){
	uibPaginationConfig.firstText='<<';
	uibPaginationConfig.previousText='<';
	uibPaginationConfig.nextText='>';
	uibPaginationConfig.lastText='>>';
	
});

app.value('$strapConfig', {
	datepicker: {
		language: 'pt-BR',
		format: 'dd/MM/yyyy'
	}
});

