package br.inf.datainfo.desafio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.inf.datainfo.desafio.model.Funcao;
import br.inf.datainfo.desafio.services.FunctionServices;

@RestController
@RequestMapping("rest/datainfo/funcao-usuario-externo")
public class FunctionController {
	
	@Autowired
	private FunctionServices service;
	
	@GetMapping("/all")
	public ResponseEntity<List<Funcao>> getAll() {
		List<Funcao> retorno = service.findAll();
        return new ResponseEntity<List<Funcao>>(retorno, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Funcao> getById(@PathVariable(value = "id") Integer id) {
		Funcao retorno = service.findById(id);
		if (retorno == null) return new ResponseEntity<Funcao>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Funcao>(retorno, HttpStatus.OK);
	}
	
}