package br.inf.datainfo.desafio.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.inf.datainfo.desafio.model.Usuario;
import br.inf.datainfo.desafio.services.UserServices;

@RestController
@RequestMapping("rest/datainfo/usuario-externo")
public class UserController {
	
	@Autowired
	private UserServices service;
	
	@GetMapping("/all")
	public ResponseEntity<List<Usuario>> getAll() {
		List<Usuario> retorno = service.findAll();
        return new ResponseEntity<List<Usuario>>(retorno, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> getById(@PathVariable(value = "id") Integer id) {
		Usuario retorno = service.findById(id);
		if (retorno == null) return new ResponseEntity<Usuario>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Usuario>(retorno, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Usuario> create(@Valid @RequestBody Usuario usuario) {
		Usuario retorno = service.save(usuario);
        return new ResponseEntity<Usuario>(retorno, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<Usuario> update(@Valid @RequestBody Usuario usuario) {
		Usuario retorno = service.update(usuario);
        return new ResponseEntity<Usuario>(retorno, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Integer> delete(@PathVariable(value = "id") Integer id) {
		service.delete(id);
        return new ResponseEntity<Integer>(id, HttpStatus.OK);
	}
	
}