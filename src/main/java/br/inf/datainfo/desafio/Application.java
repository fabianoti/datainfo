package br.inf.datainfo.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@SpringBootApplication
public class Application {

	@GetMapping(value="/")
    public String homepage() {
        return "index";
    }

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
}
