package br.inf.datainfo.desafio.services;

import java.util.ArrayList;

import br.inf.datainfo.desafio.model.Usuario;

public interface UserServices {

    ArrayList<Usuario> findAll();

    Usuario findById(Integer id);

    Usuario save(Usuario usuario);

	Usuario update(Usuario usuario);

	void delete(Integer id);

}
