package br.inf.datainfo.desafio.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inf.datainfo.desafio.model.Usuario;
import br.inf.datainfo.desafio.repository.UserRepository;

@Service
public class UserServicesImpl implements UserServices {

    @Autowired
    private UserRepository repository;

    @Override
    public ArrayList<Usuario> findAll() {
	    return (ArrayList<Usuario>) repository.findAll();
    }

    @Override
	public Usuario findById(Integer id) {
    	Usuario result = new Usuario();
    	Optional<Usuario> optional = repository.findById(id);
    	if (optional.isPresent()) result = optional.get(); 
    	return result;
	}

    @Override
	public Usuario save(Usuario usuario) {
		return repository.save(usuario);
	}
	
    @Override
	public Usuario update(Usuario usuario) {
		repository.save(usuario);
		return usuario;
	}
	
    @Override
	public void delete(Integer id){
		Optional<Usuario> usuario = repository.findById(id);
		if (usuario.isPresent()) repository.delete(usuario.get());
	}

}
