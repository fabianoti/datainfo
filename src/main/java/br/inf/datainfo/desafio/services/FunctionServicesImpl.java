package br.inf.datainfo.desafio.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.inf.datainfo.desafio.model.Funcao;
import br.inf.datainfo.desafio.repository.FunctionRepository;

@Service
public class FunctionServicesImpl implements FunctionServices {

    @Autowired
    private FunctionRepository repository;

    @Override
    public ArrayList<Funcao> findAll() {
	    return (ArrayList<Funcao>) repository.findAll();
    }

    @Override
	public Funcao findById(Integer id) {
    	Funcao result = new Funcao();
    	Optional<Funcao> optional = repository.findById(id);
    	if (optional.isPresent()) result = optional.get(); 
    	return result;
	}

}
