package br.inf.datainfo.desafio.services;

import java.util.ArrayList;

import br.inf.datainfo.desafio.model.Funcao;

public interface FunctionServices {

    ArrayList<Funcao> findAll();

    Funcao findById(Integer id);

}
