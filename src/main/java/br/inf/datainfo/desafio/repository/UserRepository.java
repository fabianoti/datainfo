package br.inf.datainfo.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.inf.datainfo.desafio.model.Usuario;

@Repository
public interface UserRepository extends JpaRepository<Usuario, Integer> {
	
}
